<?php

if (!defined('ABSPATH')) {
	exit;
}

function ianews_theme_support()
{


	add_theme_support('post-thumbnails');
	add_theme_support('custom-logo', array(
		'height'      => 100,
		'width'       => 400,
		'flex-height' => true,
		'flex-width'  => true,
		'header-text' => array('site-title', 'site-description'),
	));

	register_sidebar(
		array(
			'name'          => 'Iklan Atas',
			'id'            => 'iklan_top',
			'before_widget' => ' <div class="box mb-0 rounded-60"><div class="box-body shadow60dtk rounded-60">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => 'Iklan Bawah',
			'id'            => 'iklan_bottom',
			'before_widget' => ' <div class="box mb-2 rounded-60"><div class="box-body shadow60dtk rounded-60">',
			'after_widget'  => '</div></div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
		)
	);
	for ($i=0; $i < 4; $i++) {
		$name = 'Iklan Tengah';
		$id = 'iklan_center';
		if ($i != 0){
			$name = $name.' '.$i;
			$id = $id.'_'.$i;
		}
		register_sidebar(
			array(
				'name'          => $name,
				'id'            => $id,
				'before_widget' => ' <div class="box mb-2 rounded-60"><div class="box-body shadow60dtk rounded-60">',
				'after_widget'  => '</div></div>',
				'before_title'  => '<h2 class="rounded">',
				'after_title'   => '</h2>',
			)
		);
	}
	register_sidebar(
		array(
			'name'          => 'Iklan Kanan 1',
			'id'            => 'iklan_center_right',
			'before_widget' => ' <div class="box rounded-60 p-2 mt-3 mb-2">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => 'Iklan Kanan 2',
			'id'            => 'iklan_center_right_2',
			'before_widget' => ' <div class="box h-kontak p-2 mt-3 mb-3 bg-white rounded-60">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => 'Iklan Single',
			'id'            => 'iklan_single_right',
			'before_widget' => ' <div class="box p-2 mt-3 mb-3 rounded-60">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2 class="rounded">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name' => 'Member Of Media Gorup',
			'id' => 'member_of_media_group',
			'before_widget' => '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2 mb-2 momg">',
			'after_widget' => '</div>'
		)
	);
	register_sidebar(
		array(
			'name' => 'Ikuti Kami',
			'id' => 'ikuti_kami',
			'before_widget' => '<div class="col-xs-12 col-sm-12 col-md-6 col-lg-2 mb-2 momg">',
			'after_widget' => '</div>'
		)
	);
	register_nav_menus(array('header-menu' => 'Header Menu', 'footer-menu' => 'Footer Menu'));
}
add_action('after_setup_theme', 'ianews_theme_support');

function ianews_loadmore_ajax_handler()
{

	global $post;
	// prepare our arguments for the query
	$isSingle = $_POST['is_single'];
	if ($isSingle) {
		if ('' != $_POST['next_post_url']):
		$post_id = url_to_postid($_POST['next_post_url']);
		if ($post_id !== $_POST['current_post']){
			$post = get_post($post_id);
			setup_postdata($post);
			get_template_part('template-parts/content', 'single');
			wp_reset_postdata();
		}else{
			echo 'next_post_not_found';
		}
		endif;
		
	}
	if (!$isSingle){
		$args = json_decode(stripslashes($_POST['query']), true);
		$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
		$args['post_status'] = 'publish';
		$args['orderby'] = 'post_date';
		query_posts($args);
		if (have_posts()) :

			$is_cat = get_query_var('cat');
			// run the loop
			while (have_posts()) : the_post();
				get_template_part('template-parts/' . ($is_cat ? 'category' : 'content'), get_post_format());
			endwhile;
		endif;
	}
	die; // here we exit the script and even no wp_reset_query() required!
}
add_action('wp_ajax_loadmore', 'ianews_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'ianews_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}


/**
 * Load javascripts pada theme
 */
function infinite_scroll_ianews_js()
{
	wp_register_script('bs-4',  get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), null, true);
	wp_enqueue_script('bs-4');
	wp_register_script('simple-likes',  get_template_directory_uri() . '/assets/js/simple-likes-public.js', array('jquery'), null, true);
	wp_enqueue_script('simple-likes');
	// wp_register_script('infinite_scroll',  get_template_directory_uri() . '/assets/js/jquery.infinitescroll.js', array('jquery'), null, true);
	// wp_enqueue_script('infinite_scroll');
}
add_action('wp_enqueue_scripts', 'infinite_scroll_ianews_js');

/**
 * next and previous links
 */
function filter_single_post_pagination($output, $format, $link, $post)
{
	if (!empty($post)) {
		$label = '<i class="fa fa-backward"></i>';
		$url   = get_permalink($post->ID);
		$class = 'btn btn-primary btn-sm my-2 text-limit prev_post';
		$title = $post->post_title;
		$rel   = 'prev';

		if ('next_post_link' === current_filter()) {
			$label = '<i class="fa fa-forward"></i>';
			$class = 'btn btn-primary btn-sm my-2 text-limit float-right pagination__next';
			$rel   = 'next';
		}
		return "<a href='$url' rel='$rel' data-toggle=\"tooltip\" data-placement=\"top\" title='$title' class='$class'>$label</a>";
	}
}
add_filter('previous_post_link', 'filter_single_post_pagination', 10, 4);
add_filter('next_post_link', 'filter_single_post_pagination', 10, 4);


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/theme-options.php';

new IANEWS_Theme_options();

function ianews_get_theme_option($id = '')
{
	return IANEWS_Theme_options::get_theme_option($id);
}
function ianews_get_theme_options()
{
	return IANEWS_Theme_options::get_theme_options();
}

function ianews_get_post_view()
{
	$count = get_post_meta(get_the_ID(), 'post_views_count', true);
	return "$count";
}
function ianews_set_post_view()
{
	$key = 'post_views_count';
	$post_id = get_the_ID();
	$count = (int) get_post_meta($post_id, $key, true);
	$count++;
	update_post_meta($post_id, $key, $count);
}
function ianews_posts_column_views($columns)
{
	$columns['post_views'] = 'Views';
	return $columns;
}
function ianews_posts_custom_column_views($column)
{
	if ($column === 'post_views') {
		echo ianews_get_post_view();
	}
}
add_filter('manage_posts_columns', 'ianews_posts_column_views');
add_action('manage_posts_custom_column', 'ianews_posts_custom_column_views');


/**
 * Custom callback for outputting comments 
 *
 * @return void
 * @author Keir Whitaker
 */
function bootstrap_comment($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	?>
	<?php if ($comment->comment_approved == '1') : ?>
		<li class="media">
			<div class="media-left mr-2">
				<?php echo get_avatar($comment); ?>
			</div>
			<div class="media-body">
				<h4 class="media-heading">
					<?php comment_author_link() ?>
				</h4>
				<time>
					<a href="#comment-<?php comment_ID() ?>" pubdate>
						<?php comment_date() ?> at <?php comment_time() ?>
					</a>
				</time>
				<?php comment_text() ?>
			</div>
	<?php endif;
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Multiple selected category support
 */
require get_template_directory() . '/inc/multiple-wp-dropdown-categories.php';

/**
 * Select2 supprot
 */
require get_template_directory() . '/inc/wp-admin-select2.php';

/**
 * Bootstrap4 supprot
 */
require get_template_directory() . '/inc/wp-admin-bootstrap4.php';

// Register Custom Navigation Walker
require_once get_template_directory() . '/inc/class-wp-bootstrap-navwalker.php';

// Sosial buttons
require_once get_template_directory() . '/inc/sosial-buttons.php';

// Post Like
require_once get_template_directory() . '/inc/post-like.php';

	?>