<?php
$kategori = ianews_get_theme_option('sidebar_right_1');
$side_count = ianews_get_theme_option('sidebar_right_1_value');
$args = array(
    'numberposts' => $side_count ? $side_count : 5,
    'category' => $kategori,
    'category__not_in' => array(get_cat_ID('video'), get_cat_ID('infografik'))
);
$my_posts = get_posts($args);
?>
<h3 class="page-title px-1 mb-0 pb-0"><?= get_cat_name($kategori); ?></h3>
<div class="box box-tranding p-2 bg-white">
    <?php
    foreach ($my_posts as $post) :
        setup_postdata($post); ?>
        <div class="media mb-3">
            <?php
            $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
            if (!empty($featured_image_url)) : ?>
                <img src="<?= $featured_image_url ?>" class="mr-3 d-block w-30 h-60" alt="<?php the_title() ?>">
            <?php else : ?>
                <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="mr-3 d-block w-30 h-60" alt="<?php the_title() ?>">
            <?php endif; ?>
            <div class="media-body">
                <h5 class="mt-0 headline_title pb-1 mb-0"><a href="<?= get_the_permalink() ?>"><?= strlen(get_the_title()) > 60 ? html_cut(get_the_title(), 60) . '...' : the_title() ?></a></h5>
                <div class="sosial_button">
                    <?= ianews_get_sosial_button('fs-05'); ?>
                </div>
            </div>
        </div>
    <?php
    endforeach;
    ?>
</div>