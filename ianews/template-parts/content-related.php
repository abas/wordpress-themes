<?php
$related_args = array(
    'post_type' => 'post',
    'posts_per_page' => checkDevice() ? 6 : 3,
    'category__in' => wp_get_post_categories($post->ID),
    'post__not_in' => array($post->ID)
);
$related = new WP_Query($related_args);
if ($related->have_posts()) : ?>
    <?php while ($related->have_posts()) : $related->the_post();
        $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
    ?>
        <?php if (checkDevice()) : ?>
            <article>
                <div class="card rounded" style="width: 210px;">
                <?php else : ?>
                    <div class="col-sm-12 col-md-6 col-lg-4 mb-2 p-1">
                        <div class="card rounded">
                        <?php endif; ?>

                        <a href="<?= get_the_permalink() ?>">
                            <?php if (!empty($featured_image_url)) : ?>
                                <img src="<?= $featured_image_url ?>" class="card-img-top" alt="<?php the_title() ?>">
                            <?php else : ?>
                                <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="card-img-top" alt="<?php the_title() ?>">
                            <?php endif; ?>
                        </a>
                        <div class="card-body">
                            <h5 class="card-title"><a href="<?= get_the_permalink() ?>"><?= strlen(get_the_title()) > 40 ? html_cut(get_the_title(), 40) . '...' : the_title() ?></a></h5>
                        </div>
                        </div>
                        <?php if (checkDevice()) : ?>
            </article>
        <?php else : ?>
            </div>
        <?php endif; ?>

    <?php endwhile; ?>
<?php endif;
wp_reset_postdata(); ?>