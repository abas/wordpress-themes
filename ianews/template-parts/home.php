<section id="first">
    <div class="container p-2">
        <div class="row">
            <?php if (is_active_sidebar('iklan_top')) : ?>
                <div class="col-lg-12 mb-2">
                    <?php dynamic_sidebar('iklan_top') ?>
                </div>
            <?php endif; ?>
            <?php get_template_part('template-parts/slider') ?>
            <div class="col-lg-4 pl-1">
                <?php get_template_part('template-parts/headline'); ?>
            </div>
        </div>
    </div>
</section>
<?php
$hukum = get_category_by_slug('hukum');
if ($hukum) {
    $args = array('numberposts' => 4, 'category' => $hukum->cat_ID);
} else {
    $args = '';
}
if ($args != '') :
    $my_posts = get_posts($args);
?>
    <section id="hukum">
        <div class="container p-2">
            <div class="row">
                <div class="col-lg-8 pr-1">
                    <h3 class="page-title mb-0 pt-0">HUKUM <a href="#" class="float-right">Lihat Semua >>></a></h3>
                    <!-- hukum -->

                    <div class="row">

                        <?php
                        foreach ($my_posts as $key => $post) :
                            setup_postdata($post);
                            if (($key + 1) & 1) {
                                $pr = 'pr-0';
                            } else {
                                $pr = 'pl-1';
                            }
                        ?>
                            <div class="col-sm-12 col-md-6 col-lg-6 <?= $pr; ?> mb-2">
                                <div class="box p-1 bg-white">
                                    <div class="box-body p-2">
                                        <?php ianews_entry_author(); ?>
                                        <?php
                                        if (has_post_thumbnail($post)) : ?>
                                            <img src="<?= get_the_post_thumbnail_url($post) ?>" class="d-block w-100" alt="<?php the_title(); ?>">
                                        <?php endif;
                                        ?>
                                        <div class="pt-2">
                                            <span class="content_60dtk">60DTK | </span>
                                            <span class="content_60dtk_category">HUKUM</span>
                                        </div>
                                        <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
                                    </div>
                                    <div class="box-footer p-2 border-top-1">
                                        <div class="sosial_button fs-1">
                                            <a href="#" class="pr-3"><i class="fas fa-thumbs-up"></i> Sukai</a>
                                            <a href="#" class="pr-3"><i class="fas fa-comment-dots"></i> Komentari</a>
                                            <a href="#" class="pr-3"><i class="fas fa-share-alt"></i> Bagikan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                        ?>
                    </div>
                    <!-- endhukum -->
                </div>
                <div class="col-lg-4 pl-1">
                    <h3 class="page-title mb-0 pb-0">TRANDING</h3>
                    <?php get_template_part('template-parts/kategori', 'tranding'); ?>
                    <?php if (is_active_sidebar('iklan_center_right')) : ?>
                        <?php dynamic_sidebar('iklan_center_right') ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php
endif;
$peristiwa = get_category_by_slug('peristiwa');
if ($peristiwa) {
    $args = array('numberposts' => 4, 'category' => $peristiwa->cat_ID);
} else {
    $args = '';
}
if ($args != '') :
    $my_posts = get_posts($args);
?>
    <section id="peristiwa">
        <div class="container p-2">
            <div class="row">
                <div class="col-lg-8 pr-1">
                    <h3 class="page-title mb-0 pt-0">Peristiwa <a href="#" class="float-right">Lihat Semua >>></a></h3>
                    <!-- hukum -->

                    <div class="row">

                        <?php
                        foreach ($my_posts as $key => $post) :
                            setup_postdata($post);
                            if (($key + 1) & 1) {
                                $pr = 'pr-0';
                            } else {
                                $pr = 'pl-1';
                            }
                        ?>
                            <div class="col-sm-12 col-md-6 col-lg-6 <?= $pr; ?> mb-2">
                                <div class="box p-1 bg-white">
                                    <div class="box-body p-2">
                                        <?php ianews_entry_author(); ?>
                                        <?php
                                        if (has_post_thumbnail($post)) : ?>
                                            <img src="<?= get_the_post_thumbnail_url($post) ?>" class="d-block w-100" alt="<?php the_title(); ?>">
                                        <?php endif;
                                        ?>
                                        <div class="pt-2">
                                            <span class="content_60dtk">60DTK | </span>
                                            <span class="content_60dtk_category">PERISTIWA</span>
                                        </div>
                                        <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
                                    </div>
                                    <div class="box-footer p-2 border-top-1">
                                        <div class="sosial_button fs-1">
                                            <a href="#" class="pr-3"><i class="fas fa-thumbs-up"></i> Sukai</a>
                                            <a href="#" class="pr-3"><i class="fas fa-comment-dots"></i> Komentari</a>
                                            <a href="#" class="pr-3"><i class="fas fa-share-alt"></i> Bagikan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                        ?>
                        <?php if (is_active_sidebar('iklan_center')) : ?>
                            <div class="row clearfix">
                                <div class="col-lg-12 p-2">
                                    <?php dynamic_sidebar('iklan_center') ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <!-- endhukum -->
                </div>
            </div>
        </div>
    </section>
<?php
endif;
$infografis = get_category_by_slug('infografik');
if ($infografis) {
    $args = array('numberposts' => 4, 'category' => $infografis->cat_ID);
} else {
    $args = '';
}
if ($args != '') :
    $my_posts = get_posts($args);
?>
    <section id="infografik">
        <div class="container p-2">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Info Grafis -->
                    <h3 class="page-title mb-0 pt-0">INFografis <a href="#" class="float-right">Lihat Semua >>></a></h3>
                    <div class="row">

                        <?php
                        foreach ($my_posts as $key => $post) :
                            setup_postdata($post);
                            if (($key + 1) & 1) {
                                $pr = 'pr-1';
                            } else {
                                $pr = 'pl-1 pr-1';
                            }
                        ?>
                            <div class="col-sm-12 col-md-6 col-lg-4 <?= $pr; ?> mb-2">
                                <div class="box p-1 bg-white">
                                    <div class="box-body p-2">
                                        <?php ianews_entry_author(); ?>
                                        <?php
                                        if (has_post_thumbnail($post)) : ?>
                                            <img src="<?= get_the_post_thumbnail_url($post) ?>" class="d-block w-100" alt="<?php the_title(); ?>">
                                        <?php endif;
                                        ?>
                                        <div class="pt-2">
                                            <span class="content_60dtk">60DTK | </span>
                                            <span class="content_60dtk_category">INFOGRAFIK</span>
                                        </div>
                                        <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
                                    </div>
                                    <div class="box-footer p-2 border-top-1">
                                        <div class="sosial_button fs-1">
                                            <a href="#" class="pr-3"><i class="fas fa-thumbs-up"></i> Sukai</a>
                                            <a href="#" class="pr-3"><i class="fas fa-comment-dots"></i> Komentari</a>
                                            <a href="#" class="pr-3"><i class="fas fa-share-alt"></i> Bagikan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php
endif;
$politik = get_category_by_slug('politik');
if ($politik) {
    $args = array('numberposts' => 4, 'category' => $politik->cat_ID);
} else {
    $args = '';
}
if ($args != '') :
    $my_posts = get_posts($args);
?>
    <section id="politik">
        <div class="container p-2">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Info Grafis -->
                    <h3 class="page-title mb-0 pt-0">politik <a href="#" class="float-right">Lihat Semua >>></a></h3>
                    <div class="row">

                        <?php
                        foreach ($my_posts as $key => $post) :
                            setup_postdata($post);
                            if (($key + 1) & 1) {
                                $pr = 'pr-1';
                            } else {
                                $pr = 'pl-1 pr-1';
                            }
                        ?>
                            <div class="col-sm-12 col-md-6 col-lg-4 <?= $pr; ?> mb-2">
                                <div class="box p-1 bg-white">
                                    <div class="box-body p-2">
                                        <?php ianews_entry_author(); ?>
                                        <?php
                                        if (has_post_thumbnail($post)) : ?>
                                            <img src="<?= get_the_post_thumbnail_url($post) ?>" class="d-block w-100" alt="<?php the_title(); ?>">
                                        <?php endif;
                                        ?>
                                        <div class="pt-2">
                                            <span class="content_60dtk">60DTK | </span>
                                            <span class="content_60dtk_category">POLITIK</span>
                                        </div>
                                        <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
                                    </div>
                                    <div class="box-footer p-2 border-top-1">
                                        <div class="sosial_button fs-1">
                                            <a href="#" class="pr-3"><i class="fas fa-thumbs-up"></i> Sukai</a>
                                            <a href="#" class="pr-3"><i class="fas fa-comment-dots"></i> Komentari</a>
                                            <a href="#" class="pr-3"><i class="fas fa-share-alt"></i> Bagikan</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<section id="iklan">
    <div class="container p-2">
        <div class="row">
            <?php if (is_active_sidebar('iklan_bottom')) : ?>
                <div class="col-lg-12 mb-2">
                    <?php dynamic_sidebar('iklan_bottom') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>