
<!-- hukum -->
<div class="row">

    <?php
    $kategori = get_category_by_slug('peristiwa');
    $args = false;
    if ($kategori) {
        $args = array('numberposts' => 4, 'category' => $kategori->cat_ID);
    }
    if ($args) :
        $my_posts = get_posts($args);
    foreach ($my_posts as $key => $post) :
        setup_postdata($post);
        if (($key + 1) & 1) {
            $pr = 'pr-0';
        } else {
            $pr = 'pl-1';
        }
    ?>
        <div class="col-sm-12 col-md-6 col-lg-6 <?= $pr; ?> mb-2">
            <div class="box p-1 bg-white">
                <div class="box-body p-2">
                    <?php ianews_entry_author(); ?>
                    <?php
                    if (has_post_thumbnail($post)) : ?>
                        <img src="<?= get_the_post_thumbnail_url($post) ?>" class="d-block w-100" alt="<?php the_title(); ?>">
                    <?php endif;
                    ?>
                    <div class="pt-2">
                        <span class="content_60dtk">60DTK | </span>
                        <span class="content_60dtk_category">PERISTIWA</span>
                    </div>
                    <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
                </div>
                <div class="box-footer p-2 border-top-1">
                    <div class="sosial_button fs-1">
                        <a href="#" class="pr-3"><i class="fas fa-thumbs-up"></i> Sukai</a>
                        <a href="#" class="pr-3"><i class="fas fa-comment-dots"></i> Komentari</a>
                        <a href="#" class="pr-3"><i class="fas fa-share-alt"></i> Bagikan</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; endif; ?>
</div>
<!-- endhukum -->