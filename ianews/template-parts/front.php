<?php
get_header();
$parentID =  get_menu_parent_id('header-menu');
$childMenu = get_menu_child('header-menu', $parentID);
?>

<section id="first" class="mt-130">
    <div class="container pt-2 pr-2 pl-2 pb-0">
        <div class="row">
            <?php if (is_active_sidebar('iklan_top')) : ?>
                <div class="col-lg-12 mb-0 p-1">
                    <?php dynamic_sidebar('iklan_top') ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<section id="sliderSection" class="p-0">
    <div class="container pr-2 pl-2 pb-2 pt-0 mt-0">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-8 p-1">
                <div class="box bg-white mb-0">
                    <div class="box-body p-2">
                        <div id="slider" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php
                                $slider_args = array(
                                    'numberposts' => ianews_get_theme_option('slider_post_count') ? ianews_get_theme_option('slider_post_count') : 4,
                                    'meta_key'     => '_thumbnail_id',
                                    'category__not_in' => get_cat_ID('Infografik')
                                );
                                $slider_posts = get_posts($slider_args);
                                foreach ($slider_posts as $sp => $post) :
                                    setup_postdata($post);
                                    if ($sp == 0) {
                                        $slide_active = 'active';
                                    } else {
                                        $slide_active = '';
                                    }
                                    $category_detail = get_the_category(); ?>
                                    <div class="carousel-item <?= $slide_active ?>">
                                        <?php
                                        $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                                        if (!empty($featured_image_url)) : ?>
                                            <img src="<?= $featured_image_url ?>" class="d-block w-100" alt="<?php the_title() ?>">
                                        <?php else :  ?>
                                            <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="d-block w-100" alt="no_image">
                                        <?php endif;
                                        ?>
                                        <div class="carousel-caption">
                                            <h5><span class="highlight p-2"><?= get_the_category()[0]->cat_name ?></span></h5>
                                            <p class="slider-title"><a href="<?= get_the_permalink() ?>"><?= the_title() ?></a></p>
                                        </div>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-4 p-1">
                <?php get_template_part('template-parts/headline'); ?>
            </div>
        </div>
    </div>
</section>
<section class="p-0">
    <div class="container pl-2 pb-2 pr-2 pt-0">
        <div class="row">
            <div class="col-lg-8">
                <?php
                $nk = 0;
                for ($i = 1; $i < 6; $i++) {
                    if ($nk < 2) :
                        $sidebar_count = ianews_get_theme_option('sidebar_content_' . $i . '_value');
                        $cat = ianews_get_theme_option('sidebar_content_' . $i);
                        $args = array(
                            'numberposts' => $sidebar_count ? $sidebar_count : 4,
                            'category' => $cat,
                            'meta_key'     => '_thumbnail_id'
                        );
                        $my_posts = get_posts($args);
                        echo '<div class="row">'; ?>
                        <div class="col-sm-12 col-md-6 col-lg-12 p-1">
                            <?php echo '<h3 class="page-title mb-0"> ' . get_cat_name($cat) . '  <a href="' . get_category_link($cat) . '" class="float-right">Lihat Semua >>></a></h3>'; ?>
                        </div>
                        <?php
                        foreach ($my_posts as $pk => $post) :
                            setup_postdata($post);
                        ?>
                            <div class="col-sm-12 col-md-6 col-lg-6 mb-2 p-1">
                                <div class="box p-1 bg-white">
                                    <div class="box-body p-2">
                                        <?php ianews_entry_author(); ?>
                                        <?php
                                        $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
                                        <div class="body-image">
                                            <a href="<?= get_the_permalink() ?>">
                                                <?php if (!empty($featured_image_url)) : ?>
                                                    <img src="<?= $featured_image_url ?>" class="d-block w-100" alt="<?php the_title() ?>">
                                                <?php else : ?>
                                                    <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="d-block w-100" alt="<?php the_title() ?>">
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                        <div class="pt-2">
                                            <span class="content_60dtk">60DTK | </span>
                                            <span class="content_60dtk_category"><?= get_cat_name($cat) ?></span>
                                        </div>
                                        <div class="body-title">
                                            <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
                                        </div>
                                    </div>
                                    <div class="box-footer border-top-1 p-1">
                                        <div class="sosial_button fs-1">
                                            <?= ianews_get_sosial_button(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php if ($nk == 0 && is_active_sidebar('iklan_center')) : ?>
                            <div class="col-lg-12 clearfix p-1">
                                <?php dynamic_sidebar('iklan_center') ?>
                            </div>
                        <?php else : ?>
                            <?php if (is_active_sidebar('iklan_center_' . $nk)) : ?>
                                <div class="col-lg-12 clearfix p-1">
                                    <?php dynamic_sidebar('iklan_center_'.$nk) ?>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
            </div>
    <?php endif;
                    $nk++;
                }
    ?>
        </div>
        <div class="col-lg-4 p-1">
            <?php get_template_part('template-parts/tranding') ?>
            <?php if (is_active_sidebar('iklan_center_right')) : ?>
                <div class="bg-white">
                    <?php dynamic_sidebar('iklan_center_right') ?>
                </div>
            <?php endif; ?>
            <?php
            $kategori = ianews_get_theme_option('sidebar_right_2');
            $side_count = ianews_get_theme_option('sidebar_right_2_value');
            $args = array('numberposts' => $side_count ? $side_count : 5, 'category' => $kategori);
            $my_posts = get_posts($args); ?>
            <h3 class="page-title"><?= get_cat_name($kategori); ?></h3>
            <div class="box kanan p-2 bg-white mb-3">
                <?php
                foreach ($my_posts as $sdk => $post) :
                    setup_postdata($post); ?>
                    <div class="media <?= count($my_posts) - 1 == $sdk ? 'mb-1' : 'mb-2' ?>">
                        <?php
                        $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
                        <a href="<?= get_the_permalink() ?>" class="mr-3 d-block w-30">
                            <?php if (!empty($featured_image_url)) : ?>
                                <img src="<?= $featured_image_url ?>" class="d-block w-100" alt="<?php the_title() ?>">
                            <?php else : ?>
                                <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="d-block w-100" alt="<?php the_title() ?>">
                            <?php endif; ?>
                        </a>
                        <div class="media-body">
                            <h5 class="mt-0 headline_title pb-1 mb-0"><a href="<?= get_the_permalink() ?>"><?= strlen(get_the_title()) > 60 ? html_cut(get_the_title(), 60) . '...' : the_title() ?></a></h5>
                            <div class="sosial_button fs-05">
                                <?= ianews_get_sosial_button('fs-05'); ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach;
                ?>
            </div>
            <h3 class="page-title mt-3">redaksi</h3>
            <div class="box h-redaksi p-3 mt-2 bg-white">
                <div class="row">
                    <?php ianews_redaksi(); ?>
                </div>
            </div>
            <?php if (is_active_sidebar('iklan_center_right_2')) : ?>
                <?php dynamic_sidebar('iklan_center_right_2') ?>
            <?php endif; ?>
        </div>
        <div class="col-lg-12 p-3">
            <?php
            $nk = 0;
            for ($i = 1; $i < 6; $i++) {
                if ($nk >= 2) :
                    $sidebar_count = ianews_get_theme_option('sidebar_content_' . $i . '_value');
                    $cat = ianews_get_theme_option('sidebar_content_' . $i);
                    $args = array(
                        'numberposts' => $sidebar_count ? $sidebar_count : 4,
                        'category' => $cat,
                        'meta_key' => '_thumbnail_id'
                    );
                    $my_posts = get_posts($args);
                    echo '<div class="row">'; ?>
                    <div class="col-sm-12 col-md-6 col-lg-12 p-1">
                        <?php echo '<h3 class="page-title mb-0"> ' . get_cat_name($cat) . '  <a href="' . get_category_link($cat) . '" class="float-right">Lihat Semua >>></a></h3>'; ?>
                    </div>
                    <?php
                    if (strtoupper(get_cat_name($cat)) == 'INFOGRAFIK' && checkDevice()) : ?>
                        <div class="scrollable">
                            <div class="item">
                                <?php
                                foreach ($my_posts as $pk => $post) :
                                    setup_postdata($post);
                                    $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                                ?>
                                    <article>
                                        <div class="card rounded" style="width: 230px;">
                                            <a href="<?= get_the_permalink() ?>">
                                                <?php if (!empty($featured_image_url)) : ?>
                                                    <img src="<?= $featured_image_url ?>" class="d-block w-100 rounded" height="335px" alt="<?php the_title() ?>">
                                                <?php else : ?>
                                                    <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" height="335px" class="d-block w-100 rounded" alt="<?php the_title() ?>">
                                                <?php endif; ?>
                                            </a>
                                            <div class="card-body" style="height: 50px;">
                                                <h5 class="card-title"><a href="<?= get_the_permalink() ?>"><?= strlen(get_the_title()) > 40 ? html_cut(get_the_title(), 40) . '...' : the_title() ?></a></h5>
                                            </div>
                                        </div>
                                    </article>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php else :
                        foreach ($my_posts as $pk => $post) :
                            setup_postdata($post);
                        ?>
                            <div class="col-sm-12 col-md-6 col-lg-4 mb-2 p-1">
                                <div class="box p-1 bg-white">
                                    <div class="box-body p-2">
                                        <?php ianews_entry_author(); ?>
                                        <?php $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
                                        <div class="<?= strtolower(get_cat_name($cat)) == 'infografik' ? 'body-image-full' : 'body-image' ?>">
                                            <a href="<?= get_the_permalink() ?>">
                                                <?php if (!empty($featured_image_url)) : ?>
                                                    <img src="<?= $featured_image_url ?>" class="d-block w-100 rounded" alt="<?php the_title() ?>">
                                                <?php else : ?>
                                                    <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="d-block w-100 rounded" alt="<?php the_title() ?>">
                                                <?php endif; ?>
                                            </a>
                                        </div>
                                        <div class="pt-2">
                                            <span class="content_60dtk">60DTK | </span>
                                            <span class="content_60dtk_category"><?= get_cat_name($cat) ?></span>
                                        </div>
                                        <div class="body-title">
                                            <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
                                        </div>
                                    </div>
                                    <div class="box-footer border-top-1 p-1">
                                        <div class="sosial_button fs-1">
                                            <?= ianews_get_sosial_button(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php if (is_active_sidebar('iklan_center_'.$nk)) : ?>
                            <div class="col-lg-12 clearfix p-1">
                                <?php dynamic_sidebar('iklan_center_'.$nk) ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
        </div>
<?php endif;
                $nk++;
            }
?>
    </div>
    <div class="col-lg-12 p-1">
        <?php if (is_active_sidebar('iklan_bottom')) : ?>
            <div class="col-lg-12 clearfix p-1">
                <?php dynamic_sidebar('iklan_bottom') ?>
            </div>
        <?php endif; ?>
    </div>
    </div>
    </div>
</section>

<?php
get_footer(); ?>