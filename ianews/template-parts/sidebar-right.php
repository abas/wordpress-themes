<?php
if ($no == 1) :
    $section = 1;
    foreach (range(1, 4) as $sr) :
        $kategori = ianews_get_theme_option('sidebar_right_' . $sr);
        $side_count = ianews_get_theme_option('sidebar_right_' . $sr . '_value');
        $args = array('numberposts' => $side_count ? $side_count : 5, 'category' => $kategori);
        $my_posts = get_posts($args); ?>
        <h3 class="page-title px-2 mb-0 pb-0"><?= get_cat_name($kategori); ?></h3>
        <div class="box p-2 bg-white">
            <?php
            foreach ($my_posts as $post) :
                setup_postdata($post); ?>
                <div class="media mb-2">
                    <?php
                    $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                    if (!empty($featured_image_url)): ?>
                        <img src="<?= $featured_image_url ?>" class="mr-3 d-block w-30" alt="<?php the_title() ?>">
                    <?php endif;
                    ?>
                    <div class="media-body">
                        <h5 class="mt-0 headline_title pb-2"><a href="<?= get_the_permalink() ?>"><?php the_title() ?></a></h5>
                        <div class="sosial_button">
                            <?= ianews_get_sosial_button('fs-05'); ?>
                        </div>
                    </div>
                </div>

            <?php endforeach;
            ?>
        </div>
        <?php if ($section == 2 && is_active_sidebar('iklan_center_right')) : ?>
            <?php dynamic_sidebar('iklan_center_right') ?>
        <?php endif; ?>
<?php $section++;
    endforeach;
endif;
?>