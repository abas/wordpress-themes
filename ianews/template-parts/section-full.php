<div class="col-sm-12 col-md-6 col-lg-4 px-2 mb-2 p-1">
    <div class="box p-1 bg-white">
        <div class="box-body p-2">
            <?php ianews_entry_author(); ?>
            <?php
            $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
            if (!empty($featured_image_url)) : ?>
                <img src="<?= $featured_image_url ?>" class="d-block w-100" alt="<?php the_title() ?>">
            <?php endif;
            ?>
            <div class="pt-2">
                <span class="content_60dtk">60DTK | </span>
                <span class="content_60dtk_category">INFOGRAFIK</span>
            </div>
            <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
        </div>
        <div class="box-footer p-2 border-top-1">
            <div class="sosial_button fs-1">
                <?= ianews_get_sosial_button(); ?>
            </div>
        </div>
    </div>
</div>