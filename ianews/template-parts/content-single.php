<?php

/**
 * The template part for displaying single posts
 * ianews 1.0
 */

ianews_set_post_view();
?>
<script type="text/javascript">
	var loadmore_single = {
		"ajaxurl": '<?= site_url() ?>/wp-admin/admin-ajax.php',
		"current_post": <?= get_the_ID() ?>,
		"next_post": '<?= get_adjacent_post(true, '', false) ? get_permalink(get_adjacent_post(true, '', false)->ID) : ''; ?>',
		"prev_post": '<?= get_adjacent_post(true, '', true) ? get_permalink(get_adjacent_post(true, '', true)->ID) : ''; ?>'
	}
</script>

<div id="first">
	<div class="container">
		<div class="row">
			<?php if (is_active_sidebar('iklan_top')) : ?>
				<div class="col-lg-12 mb-2 p-1 mt-2">
					<?php dynamic_sidebar('iklan_top') ?>
				</div>
			<?php endif; ?>

			<div class="col-lg-12">
				<div class="bc-icons">
					<nav aria-label="breadcrumb">
						<ol class="breadcrumb mb-0 bg-transparent">
							<li class="breadcrumb-item"><a href="<?= esc_url(home_url('/')) ?>"><i class="fa fa-home"></i></a><i class="fas fa-angle-right mx-1 gray" aria-hidden="true"></i></li>
							<?php ianews_get_categories(); ?>
						</ol>
					</nav>
				</div>
			</div>
			<div class="<?= checkDevice() ? 'col-lg-12 px-2' : 'col-lg-8 p-1' ?>" id="mainarticle">


				<div id="ianews-content" class="mb-2">
					<div class="box p-2 bg-white">
						<div class="box-body p-2">
							<div class="row">
								<div class="col-lg-4">
									<?php ianews_entry_meta(); ?>
								</div>
							</div>
							<?php the_title('<h1 class="judul">', '</h1>'); ?>
							<?php
							$category_detail = get_the_category(get_the_ID()); //$post->ID
							$arr_cat_name = array();
							foreach ($category_detail as $cd) {
								array_push($arr_cat_name, strtoupper($cd->cat_name));
							}
							if (!in_array('INFOGRAFIK', $arr_cat_name)) {
								ianews_post_thumbnail();
								if (checkDevice()) {
									echo '<hr/>';
								}
							}
							?>
							<div class="entry-content mt-4">
								<?php
								if (checkDevice()) {
									the_content_with_ads();
								} else {
									the_content();
								}

								if ('' !== get_the_author_meta('description')) {
									get_template_part('template-parts/biography');
								}
								?>
							</div><!-- .entry-content -->

							<footer class="entry-footer">
								<?php ianews_get_tags(); ?>
								<?php
								edit_post_link(
									sprintf(
										/* translators: %s: Post title. */
										__('Edit<span class="screen-reader-text"> "%s"</span>', 'ianews'),
										get_the_title()
									),
									'<span class="edit-link">',
									'</span>'
								);
								if (comments_open() || get_comments_number()) {
									comments_template();
								}
								?>
							</footer><!-- .entry-footer -->
							</article><!-- #post-<?php the_ID(); ?> -->
						</div>
					</div>
				</div>
			</div>
			<?php if (!checkDevice()) : ?>
				<div class="col-lg-4 p-1">
					<?php if (is_active_sidebar('iklan_single_right')) : ?>
						<?php dynamic_sidebar('iklan_single_right') ?>
					<?php endif; ?>
					<hr />
					<?php get_template_part('template-parts/tranding') ?>
					<hr />
					<?php if (is_active_sidebar('iklan_center_right')) : ?>
						<?php dynamic_sidebar('iklan_center_right') ?>
					<?php endif; ?>
					<hr />
					<?php if (is_active_sidebar('iklan_center_right_2')) : ?>
						<?php dynamic_sidebar('iklan_center_right_2') ?>
					<?php endif; ?>
					<hr />
					<h3 class="page-title">Ikuti Kami</h3>
					<div class="box p-2 bg-white mb-2">
						<div class="sosmed">
							<a href="<?= ianews_get_theme_option('fb_fan_page') ?>">
								<img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/facebook.png" alt="Facebook">
							</a>
							<a href="<?= ianews_get_theme_option('twitter_url') ?>">
								<img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/instagra.png" alt="Twitter">
							</a>
							<a href="<?= ianews_get_theme_option('ig_url') ?>"><img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/twitter.png" alt="Instagram"></a>
							<a href="<?= ianews_get_theme_option('youtube_url') ?>"><img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/youtube.png" alt="Youtube"></a>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
<div id="related_post">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-6 col-lg-12 p-1">
				<?php echo '<h3 class="page-title mt-2"> Baca Juga </h3>'; ?>
			</div>
			<?php if (checkDevice()) : ?>
				<div class="scrollable">
					<div class="item">
						<?php get_template_part('template-parts/content', 'related') ?>
					</div>
				</div>
			<?php else : ?>
				<?php get_template_part('template-parts/content', 'related') ?>
			<?php endif; ?>
		</div>
	</div>
</div>