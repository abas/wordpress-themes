    <div class="box p-1 bg-white mb-3">
        <div class="box-body">
            <div id="slider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <?php
                    $args = array(
                        'numberposts' => ianews_get_theme_option('slider_post_count') ? ianews_get_theme_option('slider_post_count') : 5
                    );
                    $cat_name = '';
                    $slider = get_posts($args);
                    foreach ($slider as $key => $value) :
                        if ($key == 0) {
                            $slide_active = 'active';
                        } else {
                            $slide_active = '';
                        }
                        $category_detail = get_the_category();
                    ?>
                        <div class="carousel-item <?= $slide_active ?>">
                            <?php
                            $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID()));
                            if (!empty($featured_image_url)) : ?>
                                <img src="<?= $featured_image_url ?>" class="d-block w-100" alt="<?php the_title() ?>">
                            <?php else :  ?>
                                <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="d-block w-100" alt="no_image">
                            <?php endif;
                            ?>
                            <div class="carousel-caption">
                                <h5><span class="highlight pr-2 pl-2"><?= $category_detail[0]->cat_name ?></span></h5>
                                <p class="slider-title"><a href="<?= get_the_permalink($value->ID) ?>"><?= $value->post_title; ?></a></p>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
                <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
    </div>