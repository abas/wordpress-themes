<div class="col-sm-12 col-md-6 col-lg-6 mb-1 p-1" id="ianews-content">
    <div class="box bg-white">
        <div class="box-body p-2">
            <?php ianews_entry_author(); ?>
            <?php
            $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
            <div class="body-image">
                <a href="<?= get_the_permalink() ?>">
                    <?php if (!empty($featured_image_url)) : ?>
                        <img src="<?= $featured_image_url ?>" class="d-block w-100" alt="<?php the_title() ?>">
                    <?php else : ?>
                        <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="d-block w-100" alt="<?php the_title() ?>">
                    <?php endif; ?>
                </a>
            </div>
            <div class="pt-2">
                <span class="content_60dtk">60DTK | </span>
                <?= single_cat_title('<span class="content_60dtk_category">') ?>
            </div>
            <div class="body-title">
                <p><a href="<?= get_the_permalink() ?>"><?php the_title(); ?></a></p>
            </div>
        </div>
        <div class="box-footer p-1 border-top-1">
            <div class="sosial_button fs-1">
                <?= ianews_get_sosial_button(); ?>
            </div>
        </div>
    </div>
</div>