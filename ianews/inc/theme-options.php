<?php

if (!class_exists('IANEWS_Theme_options')) {
    class IANEWS_Theme_options
    {
        public function __construct()
        {
            // We only need to register the admin panel on the back-end
            if (is_admin()) {
                add_action('admin_menu', array('IANEWS_Theme_Options', 'add_admin_menu'));
                add_action('admin_init', array('IANEWS_Theme_Options', 'register_settings'));
            }
        }

        /**
         * Returns all theme options
         *
         * @since 1.0.0
         */
        public static function get_theme_options()
        {
            return get_option('theme_options');
        }

        /**
         * Returns single theme option
         *
         * @since 1.0.0
         */
        public static function get_theme_option($id)
        {
            $options = self::get_theme_options();
            if (isset($options[$id])) {
                return $options[$id];
            }
        }

        /**
         * Add sub menu page
         *
         * @since 1.0.0
         */
        public static function add_admin_menu()
        {
            add_menu_page(
                esc_html__('Pengaturan Tema', 'text-domain'),
                esc_html__('Pengaturan Tema', 'text-domain'),
                'manage_options',
                'theme-settings',
                array('IANEWS_Theme_Options', 'create_admin_page')
            );
        }

        /**
         * Register a setting and its sanitization callback.
         *
         * We are only registering 1 setting so we can store all options in a single option as
         * an array. You could, however, register a new setting for each option
         *
         * @since 1.0.0
         */
        public static function register_settings()
        {
            register_setting('theme_options', 'theme_options', array('IANEWS_Theme_Options', 'sanitize'));
        }

        /**
         * Sanitization callback
         *
         * @since 1.0.0
         */
        public static function sanitize($options)
        {

            // If we have options lets sanitize them
            if ($options) {

                // Is slider content from newly post
                if (!empty($options['slider_post_count'])) {
                    $options['slider_post_count'] = sanitize_text_field($options['slider_post_count']);
                } else {
                    unset($options['slider_post_count']); // Remove from options if not checked
                }

                // Is slider data post
                if (!empty($options['slider_cat_post'])) {
                    $options['slider_cat_post'] = sanitize_text_field($options['slider_cat_post']);
                } else {
                    unset($options['slider_cat_post']); // Remove from options if not checked
                }

                // Facebook
                if (!empty($options['fb_fan_page'])) {
                    $options['fb_fan_page'] = sanitize_text_field($options['fb_fan_page']);
                } else {
                    unset($options['fb_fan_page']); // Remove from options if empty
                }

                // Twitter
                if (!empty($options['twitter_url'])) {
                    $options['twitter_url'] = sanitize_text_field($options['twitter_url']);
                } else {
                    unset($options['twitter_url']); // Remove from options if empty
                }

                // Instagram
                if (!empty($options['ig_url'])) {
                    $options['ig_url'] = sanitize_text_field($options['ig_url']);
                } else {
                    unset($options['ig_url']); // Remove from options if empty
                }

                // Youtube
                if (!empty($options['youtube_url'])) {
                    $options['youtube_url'] = sanitize_text_field($options['youtube_url']);
                } else {
                    unset($options['youtube_url']); // Remove from options if empty
                }

                // Jumlah Post Tranding
                if (!empty($options['tranding_count'])) {
                    $options['tranding_count'] = sanitize_text_field($options['tranding_count']);
                } else {
                    unset($options['tranding_count']); // Remove from options if empty
                }

                // Select
                if (!empty($options['sidebar_right_section'])) {
                    $options['sidebar_right_section'] = sanitize_text_field($options['sidebar_right_section']);
                }
                // Select
                if (!empty($options['sidebar_content'])) {
                    $options['sidebar_content'] = sanitize_text_field($options['sidebar_content']);
                }

                // Options for content left
                foreach (range(1, 5) as $i) {
                    if (!empty($options['sidebar_content_' . $i])) {
                        $options['sidebar_content_' . $i] = sanitize_text_field($options['sidebar_content_' . $i]);
                    }
                    if (!empty($options['sidebar_content_' . $i . '_value'])) {
                        $options['sidebar_content_' . $i . '_value'] = sanitize_text_field($options['sidebar_content_' . $i . '_value']);
                    }
                }

                // Options for content right
                foreach (range(1, 4) as $i) {
                    if (!empty($options['sidebar_right_' . $i])) {
                        $options['sidebar_right_' . $i] = sanitize_text_field($options['sidebar_right_' . $i]);
                    }
                    if (!empty($options['sidebar_right_' . $i . '_value'])) {
                        $options['sidebar_right_' . $i . '_value'] = sanitize_text_field($options['sidebar_right_' . $i . '_value']);
                    }
                }
            }

            // Return sanitized options
            return $options;
        }

        /**
         * Settings page output
         *
         * @since 1.0.0
         */
        public static function create_admin_page()
        { ?>
            <div class="wrap">
                <form method="post" action="options.php" enctype="multipart/form-data">

                    <?php settings_fields('theme_options'); ?>
                    <h1><?php esc_html_e('Pengaturan Ianews', 'text-domain'); ?></h1>
                    <div class="row">
                        <?php include(get_template_directory() . '/inc/sidebar-content-options.php') ?>
                        <?php include(get_template_directory() . '/inc/sidebar-right-options.php') ?>
                        <div class="col-md-12 d-block w-100">
                            <div class="card">
                                <div class="card-body">

                                    <div class="form-group row">
                                        <label for="slider_cat_post" class="col-sm-3">Kategori Slider</label>
                                        <div class="col-sm-6">
                                            <?php
                                            $args = array(
                                                'selected'          => self::get_theme_option('slider_cat_post'),
                                                'name'              => 'theme_options[slider_cat_post]',
                                                'class' => 'form-control'
                                            );
                                            wp_dropdown_categories($args);
                                            ?>
                                        </div>
                                        <div class="col-sm-3">
                                            <?php $value = self::get_theme_option('slider_post_count'); ?>
                                            <input type="number" class="form-control" name="theme_options[slider_post_count]" value="<?= $value ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label"><?php esc_html_e('Tranding Count', 'text-domain'); ?></label>
                                        <div class="col-sm-8">
                                            <?php $value = self::get_theme_option('tranding_count'); ?>
                                            <input type="number" class="form-control" name="theme_options[tranding_count]" value="<?= $value ?>">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label"><?php esc_html_e('Facebook', 'text-domain'); ?></label>
                                        <div class="col-sm-8">
                                            <?php $value = self::get_theme_option('fb_fan_page'); ?>
                                            <input type="text" class="form-control" name="theme_options[fb_fan_page]" value="<?= $value ?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label"><?php esc_html_e('Twitter', 'text-domain'); ?></label>
                                        <div class="col-sm-8">
                                            <?php $value = self::get_theme_option('twitter_url'); ?>
                                            <input type="text" class="form-control" name="theme_options[twitter_url]" value="<?= $value ?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label"><?php esc_html_e('Instagram', 'text-domain'); ?></label>
                                        <div class="col-sm-8">
                                            <?php $value = self::get_theme_option('ig_url'); ?>
                                            <input type="text" class="form-control" name="theme_options[ig_url]" value="<?= $value ?>">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label"><?php esc_html_e('Kanal Youtube', 'text-domain'); ?></label>
                                        <div class="col-sm-8">
                                            <?php $value = self::get_theme_option('youtube_url'); ?>
                                            <input type="text" class="form-control" name="theme_options[youtube_url]" value="<?= $value ?>">
                                        </div>
                                    </div>
                                </div>
                            </div><!-- .card -->
                        </div>
                    </div>
                    <?php submit_button(); ?>
                </form>
            </div><!-- .wrap -->
<?php
        }
    }
}
