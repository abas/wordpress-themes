<?php
function enqueue_bs4()
{
    wp_register_style('bs4css', '//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', false, '1.0', 'all');
    wp_register_script('bs4js', '//stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', array('jquery'), '1.0', true);
    wp_enqueue_style('bs4css');
    wp_enqueue_script('bs4js');
}
add_action('admin_enqueue_scripts', 'enqueue_bs4');
?>