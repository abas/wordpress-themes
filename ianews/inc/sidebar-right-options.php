<div class="col-lg-6">
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Sidebar Right</h5>
            <?php
            foreach (range(1, 4) as $i) { ?>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label"><?php esc_html_e('Section ' . $i, 'text-domain'); ?></label>
                    <div class="col-sm-6">
                        <?php
                        $args = array(
                            'selected'          => self::get_theme_option('sidebar_right_' . $i),
                            'name'              => 'theme_options[sidebar_right_' . $i . ']',
                            'class' => 'form-control'
                        );
                        wp_dropdown_categories($args);
                        ?>
                    </div>
                    <div class="col-sm-3">
                        <?php $value = self::get_theme_option('sidebar_right_' . $i . '_value'); ?>
                        <input type="number" class="form-control" name="theme_options[sidebar_right_<?= $i ?>_value]" value="<?= $value ? $value : 4 ?>" />
                    </div>
                </div>
            <?php }
            ?>
        </div>
    </div>
</div>