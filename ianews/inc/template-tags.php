<?php

if (!function_exists('ianews_entry_meta')) :
    /**
     * Prints HTML with meta information for the categories, tags.
     *
     * Create your own ianews_entry_meta() function to override in a child theme.
     *
     * @since Ianews 1.0
     */
    function ianews_entry_meta()
    {
        if ('post' === get_post_type()) {
            ianews_entry_author();
        }

        // if (in_array(get_post_type(), array('post', 'attachment'))) {
        //     ianews_entry_date();
        // }

        $format = get_post_format();
        if (current_theme_supports('post-formats', $format)) {
            printf(
                '<span class="entry-format">%1$s<a href="%2$s">%3$s</a></span>',
                sprintf('<span class="screen-reader-text">%s </span>', _x('Format', 'Used before post format.', 'ianews')),
                esc_url(get_post_format_link($format)),
                get_post_format_string($format)
            );
        }

        // if ('post' === get_post_type()) {
        //     ianews_entry_taxonomies();
        // }

        // if (!is_singular() && !post_password_required() && (comments_open() || get_comments_number())) {
        //     echo '<span class="comments-link mt-4">';
        //     /* translators: %s: Post title. */
        //     comments_popup_link(sprintf(__('<i class="fas fa-comment-dots"></i> Komentari', 'ianews')), false, false, 'btn btn-sm btn-primary mt-2');
        //     echo '</span>';
        // }
    }
endif;


if (!function_exists('ianews_excerpt')) :
    /**
     * Displays the optional excerpt.
     *
     * Wraps the excerpt in a div element.
     *
     * Create your own ianews_excerpt() function to override in a child theme.
     *
     * @since Twenty Sixteen 1.0
     *
     * @param string $class Optional. Class string of the div element. Defaults to 'entry-summary'.
     */
    function ianews_excerpt($class = 'entry-summary')
    {
        $class = esc_attr($class);

        if (has_excerpt() || is_search()) :
?>
            <div class="<?php echo $class; ?>">
                <?php the_excerpt(); ?>
            </div><!-- .<?php echo $class; ?> -->
        <?php
        endif;
    }
endif;

if (!function_exists('ianews_post_thumbnail')) :

    function ianews_post_thumbnail()
    {
        if (post_password_required() || is_attachment() || !has_post_thumbnail()) {
            return;
        }

        if (is_singular()) :
        ?>

            <div class="post-thumbnail">
                <?php the_post_thumbnail(); ?>
                <?php if(get_post(get_post_thumbnail_id())): ?>
                <small><?= the_post_thumbnail_caption() ?></small>
                <?php endif; ?>
            </div><!-- .post-thumbnail -->

        <?php else : ?>

            <a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
                <?php the_post_thumbnail('post-thumbnail', array('alt' => the_title_attribute('echo=0'))); ?>
            </a>

        <?php
        endif; // End is_singular()
    }
endif;

if (!function_exists('ianews_entry_date')) :
    /**
     * Prints HTML with date information for current post.
     *
     * Create your own ianews_entry_date() function to override in a child theme.
     *
     * @since Twenty Sixteen 1.0
     */
    function ianews_entry_date()
    {
        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';

        if (get_the_time('U') !== get_the_modified_time('U')) {
            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
        }

        $time_string = sprintf(
            $time_string,
            esc_attr(get_the_date('c')),
            get_the_date(),
            esc_attr(get_the_modified_date('c')),
            get_the_modified_date()
        );

        printf(
            '<span class="posted-on"><span class="screen-reader-text">%1$s </span><a href="%2$s" rel="bookmark">%3$s</a></span>',
            _x('Posted on', 'Used before publish date.', 'ianews'),
            esc_url(get_permalink()),
            $time_string
        );
    }
endif;

if (!function_exists('ianews_entry_taxonomies')) :
    /**
     * Prints HTML with category and tags for current post.
     *
     * Create your own ianews_entry_taxonomies() function to override in a child theme.
     *
     * @since Twenty Sixteen 1.0
     */
    function ianews_entry_taxonomies()
    {
        $categories_list = get_the_category_list(_x(', ', 'Used between list items, there is a space after the comma.', 'ianews'));
        if ($categories_list && ianews_categorized_blog()) {
            printf(
                '<span class="cat-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
                _x('Kategori | ', 'Used before category names.', 'ianews'),
                $categories_list
            );
        }

        $tags_list = get_the_tag_list('', _x(', ', 'Used between list items, there is a space after the comma.', 'ianews'));
        if ($tags_list && !is_wp_error($tags_list)) {
            printf(
                '<span class="tags-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
                _x('Tags', 'Used before tag names.', 'ianews'),
                $tags_list
            );
        }
    }
endif;

if (!function_exists('ianews_entry_categories')) :
    function ianews_entry_categories($page = 'home')
    {
        if ($page == 'home') {
            $kat = get_the_category();
            $arr_name = array();
            foreach ($kat as $k) {
                $arr_name[] = $k->cat_name;
            }
            echo join(' | ', $arr_name);
        } else {
            $categories_list = get_the_category_list(_x(', ', 'Used between list items, there is a space after the comma.', 'ianews'));
            if ($categories_list && ianews_categorized_blog()) {
                printf(
                    '<span class="cat-links"><span class="screen-reader-text">%1$s </span>%2$s</span>',
                    _x('Home | ', 'Used before category names.', 'ianews'),
                    $categories_list
                );
            }
        }
    }
endif;

if (!function_exists('ianews_categorized_blog')) :
    /**
     * Determines whether blog/site has more than one category.
     *
     * Create your own ianews_categorized_blog() function to override in a child theme.
     *
     * @since Twenty Sixteen 1.0
     *
     * @return bool True if there is more than one category, false otherwise.
     */
    function ianews_categorized_blog()
    {
        $all_the_cool_cats = get_transient('ianews_categories');
        if (false === $all_the_cool_cats) {
            // Create an array of all the categories that are attached to posts.
            $all_the_cool_cats = get_categories(
                array(
                    'fields' => 'ids',
                    // We only need to know if there is more than one category.
                    'number' => 2,
                )
            );

            // Count the number of categories that are attached to the posts.
            $all_the_cool_cats = count($all_the_cool_cats);

            set_transient('ianews_categories', $all_the_cool_cats);
        }

        if ($all_the_cool_cats > 1 || is_preview()) {
            // This blog has more than 1 category so ianews_categorized_blog should return true.
            return true;
        } else {
            // This blog has only 1 category so ianews_categorized_blog should return false.
            return false;
        }
    }
endif;

if (!function_exists('ianews_entry_author')) :
    function ianews_entry_author($format = 'single')
    {
        $author_avatar_size = apply_filters('ianews_author_avatar_size', 49);
        if ($format == 'single') {
            printf(
                '<div class="row">
                <div class="col-lg-12">
                <div class="media mb-2 imgauthorsinggle">%1$s
                <div class="media-body">
                <h5 class="mt-0 content_author_name mb-0 pb-0 pt-2"><a class="url fn n" href="%2$s">%3$s</a></h5>
                <small class="content_post_date pt-0">%4$s %5$s</small>
                </div></div></div>
                </div>',
                get_avatar(get_the_author_meta('user_email'), $author_avatar_size),
                esc_url(get_author_posts_url(get_the_author_meta('ID'))),
                get_the_author(),
                get_the_date(),
                get_the_time()
            );
        } else {
            printf(
                '<div class="media mb-2">%1$s<div class="media-body"><h5 class="mt-0 content_author_name mb-0 pb-0 pt-2"><a class="url fn n" href="%2$s">%3$s</a></h5><small class="content_post_date pt-0">%4$s</small></div></div>',
                get_avatar(get_the_author_meta('user_email'), $author_avatar_size),
                esc_url(get_author_posts_url(get_the_author_meta('ID'))),
                get_the_author(),
                get_the_date()
            );
        }
    }
endif;

if (!function_exists('ianews_get_tags')) {
    function ianews_get_tags()
    {
        $tags_list = get_the_tag_list('', _x(', ', 'Used between list items, there is a space after the comma.', 'ianews'));
        if ($tags_list && !is_wp_error($tags_list)) {
            $tags = get_the_tags();
            $badge_class = array(
                '0' => 'badge-primary',
                '1' => 'badge-secondary',
                '2' => 'badge-success',
                '3' => 'badge-danger',
                '4' => 'badge-warning',
                '5' => 'badge-info'
            );
            $no = 0;
            foreach ($tags as $tag) {
                if (isset($badge_class[$no])) {
                    echo '<span class="badge ' . $badge_class[$no] . '"><a href="'.get_tag_link($tag->term_id).'">' . $tag->name . '</a></span> ';
                } else {
                    $no = rand(0, 5);
                }
                $no++;
            }
        }
    }
}

if (!function_exists('ianews_get_categories')) {
    function ianews_get_categories()
    {
        $categories = get_the_category();
        $count_cat = count($categories);
        foreach ($categories as $k => $cat) {
            if ($k == $count_cat-1){
                echo '<li class="breadcrumb-item"><a href="' . get_category_link($cat) . '" class="gray">' . $cat->cat_name . '</a></li>';
            }else{
                echo '<li class="breadcrumb-item"><a href="' . get_category_link($cat) . '" class="gray">' . $cat->cat_name . '</a><i class="fas fa-angle-right mx-1 gray" aria-hidden="true"></i></li>';
            }
        }
    }
}

if (!function_exists('ianews_get_sosial_button')) {
    function ianews_get_sosial_button($btn_class = '', $is_comment = null)
    { ?>
        <?php echo get_simple_likes_button(get_the_ID(), $is_comment); ?>
        <a href="<?= get_the_permalink() ?>" class=""><i class="fas fa-comment-dots"></i> Komentari</a>
        <button type="button" class="btn btn-sm sharebutton <?= $btn_class ?>" data-toggle="popover"><i class="fas fa-share-alt"></i> Bagikan</button>
        <span class="share-box">
            <a class="twitter" href="https://twitter.com/intent/tweet?text=<?php the_title() ?>&url=<?= get_the_permalink() ?>" target="_blank">
                <img src="<?= get_template_directory_uri() ?>/assets/img/icon-twitter-white.png" alt="Twitter"></a>
            <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>" target="_blank">
                <img src="<?= get_template_directory_uri() ?>/assets/img/icon-facebook-white.png" alt="Facebook"></a>
            <a class="share-whatsapp" href="whatsapp://send?text='<?php echo get_the_permalink(); ?>' &t=<?= get_the_permalink() ?> target=" _blank">
                <svg id="whatsapp" data-name="whatsapp" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
                    <title>whatsapp</title>
                    <path d="m12 0c-6.6 0-12 5.4-12 12s5.4 12 12 12 12-5.4 12-12-5.4-12-12-12zm0 3.8c2.2 0 4.2 0.9 5.7 2.4 1.6 1.5 2.4 3.6 2.5 5.7 0 4.5-3.6 8.1-8.1 8.1-1.4 0-2.7-0.4-3.9-1l-4.4 1.1 1.2-4.2c-0.8-1.2-1.1-2.6-1.1-4 0-4.5 3.6-8.1 8.1-8.1zm0.1 1.5c-3.7 0-6.7 3-6.7 6.7 0 1.3 0.3 2.5 1 3.6l0.1 0.3-0.7 2.4 2.5-0.7 0.3 0.099c1 0.7 2.2 1 3.4 1 3.7 0 6.8-3 6.9-6.6 0-1.8-0.7-3.5-2-4.8s-3-2-4.8-2zm-3 2.9h0.4c0.2 0 0.4-0.099 0.5 0.3s0.5 1.5 0.6 1.7 0.1 0.2 0 0.3-0.1 0.2-0.2 0.3l-0.3 0.3c-0.1 0.1-0.2 0.2-0.1 0.4 0.2 0.2 0.6 0.9 1.2 1.4 0.7 0.7 1.4 0.9 1.6 1 0.2 0 0.3 0.001 0.4-0.099s0.5-0.6 0.6-0.8c0.2-0.2 0.3-0.2 0.5-0.1l1.4 0.7c0.2 0.1 0.3 0.2 0.5 0.3 0 0.1 0.1 0.5-0.099 1s-1 0.9-1.4 1c-0.3 0-0.8 0.001-1.3-0.099-0.3-0.1-0.7-0.2-1.2-0.4-2.1-0.9-3.4-3-3.5-3.1s-0.8-1.1-0.8-2.1c0-1 0.5-1.5 0.7-1.7s0.4-0.3 0.5-0.3z" />
                </svg>
            </a>
        </span>
<?php }
}

if (!function_exists('remove_img_attribute')) {
    function remove_img_attribute($html)
    {
        $html = preg_replace('/(width|height)="\d*"\s/', "", $html);
        return $html;
    }
}

if (!function_exists('loadmore')) {
    function loadmore($pages = '', $range = 2)
    {
        $showitems = ($range * 2) + 1;

        global $paged;
        if (empty($paged)) $paged = 1;

        if ($pages == '') {
            global $wp_query;
            $pages = $wp_query->max_num_pages;
            if (!$pages) {
                $pages = 1;
            }
        }

        if (1 != $pages) {
            echo "<div class='pagination'>";
            if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link(1) . "'>&laquo;</a>";
            if ($paged > 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo;</a>";

            for ($i = 1; $i <= $pages; $i++) {
                if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                    echo ($paged == $i) ? "<span class='current'>" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a>";
                }
            }

            if ($paged < $pages && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged + 1) . "'>&rsaquo;</a>";
            if ($paged < $pages - 1 &&  $paged + $range - 1 < $pages && $showitems < $pages) echo "<a href='" . get_pagenum_link($pages) . "'>&raquo;</a>";
            echo "</div>\n";
        }
    }
}

if (!function_exists('get_menu_child')) {
    function get_menu_child($menuLocation, $parentID, $depth = true)
    {
        $menuLocations = get_nav_menu_locations();
        $menuID = $menuLocations[$menuLocation];
        $menuItems = wp_get_nav_menu_items($menuID, array('order' => 'DESC'));
        $menu_item_list = array();
        foreach ($menuItems as $menuItem) {
            if ($menuItem->menu_item_parent == $parentID) {
                $menu_item_list[] = $menuItem;
                if ($depth) {
                    if ($child = get_menu_child($menuLocation, $menuItem->ID)) {
                        $menu_item_list = array_merge($menu_item_list, $child);
                    }
                }
            }
        }
        return $menu_item_list;
    }
}

if (!function_exists('get_menu_parent_id')) {
    function get_menu_parent_id($menuLocation)
    {
        $menuLocations = get_nav_menu_locations();
        $menuID = $menuLocations[$menuLocation];
        $menuItems = wp_get_nav_menu_items($menuID, array('order' => 'DESC'));
        // var_dump($menuItems);
        $parentID = null;
        foreach ($menuItems as $menuItem) {
            if (strtolower($menuItem->title) == 'foto') {
                $parentID = $menuItem->ID;
            }
        }
        return $parentID;
    }
}

if (!function_exists('the_cat_name')) {
    function the_cat_name($cat_name = '')
    {
        $category_detail = get_the_category(the_ID());
        $category_name = '';
        foreach ($category_detail as $key => $value) {
            if ($cat_name == $value->cat_name) {
                $category_name = $value->cat_name;
            }
        }
        echo $category_name;
    }
}

if (!function_exists('html_cut')) {
    function html_cut($text, $max_length)
    {
        $tags   = array();
        $result = "";

        $is_open   = false;
        $grab_open = false;
        $is_close  = false;
        $in_double_quotes = false;
        $in_single_quotes = false;
        $tag = "";

        $i = 0;
        $stripped = 0;

        $stripped_text = strip_tags($text);

        while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length) {
            $symbol  = $text{
                $i};
            $result .= $symbol;

            switch ($symbol) {
                case '<':
                    $is_open   = true;
                    $grab_open = true;
                    break;

                case '"':
                    if ($in_double_quotes)
                        $in_double_quotes = false;
                    else
                        $in_double_quotes = true;

                    break;

                case "'":
                    if ($in_single_quotes)
                        $in_single_quotes = false;
                    else
                        $in_single_quotes = true;

                    break;

                case '/':
                    if ($is_open && !$in_double_quotes && !$in_single_quotes) {
                        $is_close  = true;
                        $is_open   = false;
                        $grab_open = false;
                    }

                    break;

                case ' ':
                    if ($is_open)
                        $grab_open = false;
                    else
                        $stripped++;

                    break;

                case '>':
                    if ($is_open) {
                        $is_open   = false;
                        $grab_open = false;
                        array_push($tags, $tag);
                        $tag = "";
                    } else if ($is_close) {
                        $is_close = false;
                        array_pop($tags);
                        $tag = "";
                    }

                    break;

                default:
                    if ($grab_open || $is_close)
                        $tag .= $symbol;

                    if (!$is_open && !$is_close)
                        $stripped++;
            }

            $i++;
        }

        while ($tags)
            $result .= "</" . array_pop($tags) . ">";

        return $result;
    }
}

if (!function_exists('ianews_redaksi')) {
    function ianews_redaksi()
    {
        $args = array(
            'orderby' => 'user_nicename',
            'order'   => 'ASC'
        );
        $users = get_users($args);
        foreach ($users as $user) {
            echo '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 redaksi-pict">';
            echo '<img src="' . get_avatar_url($user->ID) . '" class="mx-auto d-block rounded-circle" alt="">';
            echo '<div class="redaksi">';
            echo '<p class="text-center fs-09 fc-60 text-bold ff-bn">' . esc_html($user->display_name) . ' <br/><small>' . join(',', $user->roles) . '</small></p>';
            echo '</div></div>';
        }
    }
}

if (!function_exists('checkDevice')) {
    function checkDevice()
    {
        // checkDevice() : checks if user device is phone, tablet, or desktop
        // RETURNS 0 for desktop, 1 for mobile, 2 for tablets

        if (is_numeric(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "mobile"))) {
            return is_numeric(strpos(strtolower($_SERVER['HTTP_USER_AGENT']), "tablet")) ? 2 : 1;
        } else {
            return 0;
        }
    }
}


if (!function_exists('ianews_mega_menu')) {
    function ianews_mega_menu()
    {
        $menu_name = 'header-menu';
        $locations = get_nav_menu_locations();
        $menu = wp_get_nav_menu_object($locations[$menu_name]);
        $primary_menu = wp_get_nav_menu_items($menu->term_id, array('depth' => 0));
        foreach ($primary_menu as $pmk => $pmv) : ?>
            <?php if ($pmv->menu_item_parent == 0) : ?>
                <div class="column">
                    <h3><a href="<?= $pmv->url ?>"><?= $pmv->title ?></a></h3>
                    <?php if(is_array(get_menu_child($menu_name,$pmv->ID))): ?>
                    <?php foreach(get_menu_child($menu_name,$pmv->ID) as $pck => $pcv): ?>
                        <a href="<?= $pcv->url ?>"><?= $pcv->title ?></a>
                    <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php endforeach; 
    }
}

if (!function_exists('reset_embed')){
    function reset_embed($embed_size)
    {
        // Applies for all embedded media 
        /*$embed_size['width'] = 827;*/ // Adjust the width of the player 
        // $embed_size['height'] = 120; // Adjust the height of the player 
        unset($embed_size['width']);
        unset($embed_size['height']);
        return $embed_size; // Return new size 
    }
}

if (!function_exists('the_content_with_ads')){
    function the_content_with_ads()
    {
        ob_start();
        the_content();
        $content = ob_get_contents();
        ob_end_clean();
        $ad_code = '<div class="content-ads">';
        $ad_code .= '<h3 class="content-ads-title">ADVERTISMENT</h3>';
        if (is_active_sidebar('iklan_single_right')) {
            ob_start();
            dynamic_sidebar('iklan_single_right');
            $ad_code .= ob_get_contents();
            ob_end_clean();
        }
        $ad_code .= '</div>';

        $ad_code2 = '<div class="content-ads">';
        $ad_code2 .= '<h3 class="content-ads-title">ADVERTISMENT</h3>';
        if (is_active_sidebar('iklan_center_right')) {
            ob_start();
            dynamic_sidebar('iklan_center_right');
            $ad_code2 .= ob_get_contents();
            ob_end_clean();
        }
        $ad_code2 .= '</div>';


        $closing_p = '</p>';
        $paragraphs = explode($closing_p, $content);
        foreach ($paragraphs as $index => $paragraph) {

            if (trim($paragraph)) {
                $paragraphs[$index] .= $closing_p;
            }

            if (2 == $index + 1) {
                $paragraphs[$index] .= $ad_code;
            }

            if (4 == $index + 1){
                $paragraphs[$index] .= $ad_code2;
            }
        }
        $content = implode('', $paragraphs);
        echo $content;
    }
}

if (!function_exists('show_post_thumbnail_from_content')){
    function show_post_thumbnail_from_content($id)
    {
        $content = '';
        $attachments = get_children(array(
            'post_parent' => $id,
            'post_status' => 'inherit',
            'post_type' => 'attachment',
            'post_mime_type' => 'image',
            'order' => 'ASC',
            'orderby' => 'menu_order'
        ));
        print_r($attachments);
        if ($attachments) {
            foreach ($attachments as $attachment) {
                set_post_thumbnail($id, $attachment->ID);
                break;
            }
            // display the featured image
            $content = the_post_thumbnail();
        }
        echo $content;
    }
}