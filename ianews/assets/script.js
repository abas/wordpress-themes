$(document).ready(function () {
    initNavigation();
    $(".sharebutton").click(function(){
        if ($(this).parent('div').hasClass('clicked')) {
            $(this).parent('div').removeClass('clicked');
        }else{
            $(this).parent('div').addClass('clicked');
        }
    });


    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        $("#menu-coba .dropdown-toggle").attr("data-toggle","dropdown");
        renderTime('clockDisplayMobile');
    }else{
        renderTime();
    }

    var stickyToggle = function (sticky, stickyWrapper, scrollElement) {
        var stickyHeight = sticky.outerHeight();
        var stickyTop = stickyWrapper.offset().top;
        if (scrollElement.scrollTop() >= stickyTop) {
            stickyWrapper.height(stickyHeight);
            sticky.addClass("is-sticky");
        }
        else {
            sticky.removeClass("is-sticky");
            stickyWrapper.height('auto');
        }
    };

    // Find all data-toggle="sticky-onscroll" elements
    $('#first-nav').each(function () {
        var sticky = $(this);
        var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
        sticky.before(stickyWrapper);
        sticky.addClass('sticky');

        // Scroll & resize events
        $(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function () {
            stickyToggle(sticky, stickyWrapper, $(this));
        });

        // On page load
        stickyToggle(sticky, stickyWrapper, $(window));
    });


    //Get the button:
    mybutton = document.getElementById("toTop");

    // When the user scrolls down 20px from the top of the document, show the button
    window.onscroll = function () { scrollFunction() };

});

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        mybutton.style.display = "block";
    } else {
        mybutton.style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}

function initNavigation() {
    $('.actions-item--navigation, .overlay').click(function () {
        $('.overlay').fadeToggle(300);
        $('.navbar-menu').fadeToggle(300);
        $('.icon--navigation').toggleClass('open');
        $('body').toggleClass('no-scroll');
        $(this).find('span').toggleClass('fa-times');
        $("#first-nav").toggleClass('fixed-top');
        
    });
    $(document).keyup(function (e) {
        if (e.keyCode == 27) {
            $('.overlay').fadeOut(300);
            $('.navbar-menu').fadeOut(300);
            $('.icon--navigation').removeClass('open');
            $('body').removeClass('no-scroll');
        }
    });
}
function renderTime(elmID ='clockDisplay') {
    // Date
    var mydate = new Date();
    var year = mydate.getYear();
    if (year < 1000) {
            year +=1900;
        }
    var day = mydate.getDay();
    var month = mydate.getMonth();
    var daym = mydate.getDate();
    var dayarray = new Array("Minggu,", "Senin,", "Selasa,", "Rabu,", "Kamis,", "Jum'at,", "Sabtu,");
    var montharray = new Array("Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des");
    
    // Time
    var currentTime = new Date();
    var h = currentTime.getHours();
    var m = currentTime.getMinutes();
    var s = currentTime.getSeconds();
        if (h == 24) {
            h = 0;
        } else if (h > 12) {
            h = h - 0;
        }
        
        if (h < 10){
            h = "0" + h;
        }
        
        if (m < 10){
            m = "0" + m;
        }
        
        if (s < 10){
            s = "0" + s;
        }
        
        var myClock = document.getElementById(elmID);
        myClock.textContent = "" +dayarray[day]+ " " +daym+ " " +montharray[month]+ " " +year+ " | " +h+ ":"
        +m;
        myClock.innerText = "" +dayarray[day]+ " " +daym+ " " +montharray[month]+ " " +year+ " | " +h+ ":"
        +m;
        
        setTimeout("renderTime()", 1000);
}