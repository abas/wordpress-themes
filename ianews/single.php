<?php
$classMain = 'col-lg-8 [-1';
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>
<section id="single-article">
	<?php
	while (have_posts()) :
		the_post();
		get_template_part('template-parts/content', 'single');
	?>
	<?php
	endwhile;
	?>
</section>

<?php get_footer(); ?>