<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= get_template_directory_uri() ?>/style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://kit.fontawesome.com/dd3e9f62d0.js" crossorigin="anonymous"></script>
    <title><?php bloginfo('name') ?> | <?php bloginfo('description') ?></title>
    <?php wp_head() ?>
</head>

<body <?php body_class() ?>>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-nav60dtk" id="first-nav">
            <div class="container">
                <a class="navbar-brand" href="<?= esc_url(home_url('/')) ?>">
                    <?php $custom_logo_id = get_theme_mod('custom_logo');
                    $image_logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    ?>
                    <img src="<?= $image_logo[0]; ?>" alt="<?php bloginfo('name') ?>" class="img-fluid">
                </a>

                <form action="<?= esc_url(home_url('/')) ?>" class="mx-2 my-auto d-inline w-100 text-white" role="search" method="GET">
                    <div class="input-group text-white">
                        <input type="search" value="<?php echo get_search_query(); ?>" class="form-control border border-right-0 bg-transparent text-white" name="s" placeholder="Search...">
                        <span class="input-group-append">
                            <button class="btn btn-outline-secondary text-white border border-left-0" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </span>
                    </div>
                </form>

                <a onload="renderTime();" class="mx-2 col-xs-12 col-sm-12 col-md-6 col-lg-3">
                    <div id="clockDisplay" class="tampilwaktu"></div>
                </a>
                <button class="actions-item--navigation" type="button" role="button" style="background-color: transparent; border: 0px;">
                    <span class="fas fa-bars fa-2x"></span>
                </button>
            </div>
        </nav>
        <div class="navbar-menu" style="display: none;">
            <nav class="container navbar-menu-container">
                <div class="row p-2">
                    <div class="col-lg-12">
                        <div class="primary-menu">
                            <div class="row">
                                <form action="<?= esc_url(home_url('/')) ?>" class="mx-2 d-inline w-100 text-white" role="search" method="GET">
                                    <div class="input-group text-white">
                                        <input type="search" value="<?php echo get_search_query(); ?>" class="form-control border border-right-0 bg-transparent text-white" name="s" placeholder="Search...">
                                        <span class="input-group-append">
                                            <button class="btn btn-outline-secondary text-white border border-left-0" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                                <?php ianews_mega_menu() ?>
                            </div>
                        </div>
                        <div class="ianews-mmp">
                            <span class="text-white text-bold">Sosial Media</span>
                            <div class="row">
                                <div class="sosmed">
                                    <a href="<?= ianews_get_theme_option('fb_fan_page') ?>">
                                        <img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/facebook.png" alt="Facebook">
                                    </a>
                                    <a href="<?= ianews_get_theme_option('twitter_url') ?>">
                                        <img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/instagra.png" alt="Twitter">
                                    </a>
                                    <a href="<?= ianews_get_theme_option('ig_url') ?>"><img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/twitter.png" alt="Instagram"></a>
                                    <a href="<?= ianews_get_theme_option('youtube_url') ?>"><img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/youtube.png" alt="Youtube"></a>
                                </div>
                            </div>
                        </div>
                        <div class="ianews-mmp">
                            <span class="text-white text-bold">Member Of Media Publisher Gorontalo</span>
                            <div class="row pt-4">
                                <?php if (is_active_sidebar('member_of_media_group')) : ?>
                                    <?php remove_img_attribute(dynamic_sidebar('member_of_media_group')) ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
        <div class="overlay" style="display: none;"></div>
        <nav class="navbar navbar-expand-md navbar-light bg-nav60dtk mobile-nav fixed-top">
            <div class="container">
                <a class="navbar-brand text-white text-bold m-logo" href="<?= esc_url(home_url('/')) ?>">
                    <?php $custom_logo_id = get_theme_mod('custom_logo');
                    $image_logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    ?>
                    <img src="<?= $image_logo[0]; ?>" alt="<?php bloginfo('name') ?>" width="145" height="37">
                </a>
                <a onload="renderTime('clockDisplayMobile');" class="">
                    <div id="clockDisplayMobile" class="tampilwaktu"></div>
                </a>
                <button class="actions-item--navigation" type="button" role="button" style="background-color: transparent; border: 0px;">
                    <span class="fas fa-bars"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarMobileMenu">
                    <div class="mobileNavHeader">
                        <a class="navbar-brand text-white text-bold" href="<?= esc_url(home_url('/')) ?>">
                            <?php $custom_logo_id = get_theme_mod('custom_logo');
                            $image_logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                            ?>
                            <img src="<?= $image_logo[0]; ?>" alt="<?php bloginfo('name') ?>" class="img-fluid" width="145" height="37">
                        </a>
                        <button class="navbar-toggler float-right mt-2 mr-2" type="button" data-toggle="collapse" id="toggle" data-target="#navbarMobileMenu" aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation" style="background-color: transparent; border: 0px;">
                            <span class="fas fa-times text-white"></span>
                        </button>
                    </div>
                    <form action="<?= esc_url(home_url('/')) ?>" class="my-auto d-inline w-100 text-white mr-2" role="search" method="GET">
                        <div class="input-group text-white">
                            <input type="search" value="<?php echo get_search_query(); ?>" class="form-control border border-right-0 bg-transparent text-white" name="s" placeholder="Search...">
                            <span class="input-group-append">
                                <button class="btn btn-outline-secondary text-white border border-left-0" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'header-menu',
                        'depth' => 2,
                        'container' => false,
                        'menu_class' => 'navbar-nav p-2 mr-auto',
                        'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                        'walker' => new WP_Bootstrap_Navwalker(),
                    ));
                    ?>
                </div>
            </div>
        </nav>
    </header>