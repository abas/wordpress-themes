<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'footer-menu',
                    'depth' => 2,
                    'container' => false,
                    'menu_class' => 'footer-menu'
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<button onclick="topFunction()" id="toTop" title="Go to top"><i class="fa fa-arrow-up"></i></button>
<script type='text/javascript'>
    /* <![CDATA[ */
    var simpleLikes = {
        "ajaxurl": "<?= esc_url(home_url('/')) ?>wp-admin/admin-ajax.php",
        "like": "Sukai",
        "unlike": "Tidak Suka"
    };
    /* ]]> */
</script>
<?php wp_footer() ?>
<?php if (is_single()) : ?>
    <script type="text/javascript">
        jQuery(function($) {
            var canBeLoaded = true, // this param allows to initiate the AJAX call only if necessary
                bottomOffset = 900; // the distance (in px) from the page bottom when you want to load more posts
            var prev_url = '';
            var next_url = '';
            var lastScrollTop = 0;
            console.log($("#mainarticle .judul").val());
            $(window).scroll(function() {
                var data = {
                    'action': 'loadmore',
                    'next_post_url': loadmore_single.next_post,
                    'current_post': loadmore_single.current_post,
                    'is_single': '1'
                };
                var st = $(document).scrollTop();
                if (st > lastScrollTop) {
                    if ($(document).scrollTop() > ($(document).height() - bottomOffset) && canBeLoaded == true) {
                        $.ajax({
                            url: loadmore_single.ajaxurl,
                            data: data,
                            type: 'POST',
                            beforeSend: function(xhr) {
                                canBeLoaded = false;
                            },
                            success: function(data) {
                                if (data == 'next_post_not_found' || '0' == data) {
                                    canBeLoaded = false;
                                } else {
                                    next_url = loadmore_single.next_post;
                                    $("#single-article").append(data);
                                    canBeLoaded = true;
                                }
                            }
                        });
                    }
                    if (next_url != '') {
                        window.history.pushState("object or string", "Title", next_url);
                    }
                } else {
                    window.history.pushState("object or string", "Title", prev_url);
                    prev_url = loadmore_single.prev_post;
                }
                lastScrollTop = st;
            });
        });
    </script>
<?php endif; ?>
<?php if (is_category()) : global $wp_query; ?>
    <script type="text/javascript">
        var ianews_loadmore_params = {
            "ajaxurl": '<?= site_url() ?>/wp-admin/admin-ajax.php',
            "posts": `<?= json_encode($wp_query->query_vars) ?>`, // everything about your loop is here
            "current_page": <?= get_query_var('paged') ? get_query_var('paged') : 1 ?>,
            "max_page": `<?= $wp_query->max_num_pages ?>`
        }
    </script>
    <script type="text/javascript">
        jQuery(function($) {
            var canBeLoaded = true, // this param allows to initiate the AJAX call only if necessary
                bottomOffset = 2000; // the distance (in px) from the page bottom when you want to load more posts
            $(window).scroll(function() {
                var data = {
                    'action': 'loadmore',
                    'query': ianews_loadmore_params.posts,
                    'page': ianews_loadmore_params.current_page,
                    'is_single': '0'
                };
                if ($(document).scrollTop() > ($(document).height() - bottomOffset) && canBeLoaded == true) {
                    $.ajax({
                        url: ianews_loadmore_params.ajaxurl,
                        data: data,
                        type: 'POST',
                        beforeSend: function(xhr) {
                            canBeLoaded = false;
                        },
                        success: function(data) {
                            if (data) {
                                $('#ianews-content:last-of-type').after(data); // where to insert posts
                                canBeLoaded = true; // the ajax is completed, now we can run it again
                                ianews_loadmore_params.current_page++;
                            };
                        }
                    });
                }
            });
        });
    </script>
<?php endif; ?>
<script src="<?= get_template_directory_uri() ?>/assets/script.js" type="text/javascript"></script>
</body>

</html>