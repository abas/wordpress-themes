<?php
get_header(); ?>
<?php
if (is_search()) { ?>
    <section id="first" class="mt-130">
        <div class="container p-2">
            <div class="row">
                <?php if (is_active_sidebar('iklan_top')) : ?>
                    <div class="col-lg-12 mb-2 p-1">
                        <?php dynamic_sidebar('iklan_top') ?>
                    </div>
                <?php endif; ?>
                <div class="col-lg-12 p-1">
                    <div class="card">
                        <div class="card-body">
                            <span class="content_60dtk">Hasil Pencarian </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 <?= checkDevice() ? '' : 'pr-2' ?>" id="mainarticle">
                    <div class="row">

                        <?php if ($wp_query->found_posts) { ?>
                            <?php while (have_posts()) :
                                the_post();
                                get_template_part('template-parts/content', 'search');
                            endwhile; ?>
                        <?php } else {
                            echo '<div class="alert alert-warning" role="alert"> Tidak ditemukan</div>';
                        } ?>
                    </div>
                </div>
                <div class="col-lg-4 <?= checkDevice() ? 'p-1' : 'pl-2' ?>">
                    <?php if (is_active_sidebar('iklan_single_right')) : ?>
                        <div class="bg-white">
                            <?php dynamic_sidebar('iklan_single_right') ?>
                        </div>
                    <?php endif; ?>
                    <?php get_template_part('template-parts/tranding') ?>
                    <?php if (is_active_sidebar('iklan_center_right')) : ?>
                        <div class="bg-white">
                            <?php dynamic_sidebar('iklan_center_right') ?>
                        </div>
                    <?php endif; ?>
                    <?php if (is_active_sidebar('iklan_center_right_2')) : ?>
                        <?php dynamic_sidebar('iklan_center_right_2') ?>
                    <?php endif; ?>
                    <h3 class="page-title">Ikuti Kami</h3>
                    <div class="box p-2 bg-white mb-2">
                        <div class="sosmed">
                            <a href="<?= ianews_get_theme_option('fb_fan_page') ?>">
                                <img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/facebook.png" alt="Facebook">
                            </a>
                            <a href="<?= ianews_get_theme_option('twitter_url') ?>">
                                <img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/instagra.png" alt="Twitter">
                            </a>
                            <a href="<?= ianews_get_theme_option('ig_url') ?>"><img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/twitter.png" alt="Instagram"></a>
                            <a href="<?= ianews_get_theme_option('youtube_url') ?>"><img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/youtube.png" alt="Youtube"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } else {
    if (have_posts()) :
        if (is_front_page()) {
            get_template_part('template-parts/front');
        } else {
            $is_cat = get_query_var('cat'); ?>
            <section id="first" class="mt-130">
                <div class="container p-2">
                    <div class="row">
                        <?php if (is_active_sidebar('iklan_top')) : ?>
                            <div class="col-lg-12 mb-2 p-1">
                                <?php dynamic_sidebar('iklan_top') ?>
                            </div>
                        <?php endif; ?>
                        <?php if ($is_cat) : ?>
                            <?php else : if (!checkDevice()) :  ?>
                                <div class="col-lg-12 p-1">
                                    <div class="bc-icons">
                                        <nav aria-label="breadcrumb">
                                            <ol class="breadcrumb mb-0 p-0 bg-transparent">
                                                <li class="breadcrumb-item"><a href="<?= esc_url(home_url('/')) ?>">Home</a><i class="fas fa-angle-right mx-1 gray" aria-hidden="true"></i></li>
                                                <?php ianews_get_categories(); ?>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                        <?php endif;
                        endif; ?>
                        <div class="col-lg-8 <?= checkDevice() ? '' : 'pr-2' ?>" id="mainarticle">
                            <?php
                            if ($is_cat) { ?>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-12 p-1">
                                        <?php echo single_cat_title('<h3 class="page-title mb-0">'); ?>
                                    </div>
                                    <?php
                                    while (have_posts()) {
                                        the_post();
                                        get_template_part('template-parts/category', get_post_format());
                                    } ?>
                                </div>
                            <?php } else { ?>
                                <div class="row">
                                    <?php
                                    while (have_posts()) :
                                        the_post();
                                        get_template_part('template-parts/content', get_post_format());
                                    endwhile; ?>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="col-lg-4 <?= checkDevice() ? 'p-1' : 'pl-2' ?>">
                            <?php get_template_part('template-parts/tranding') ?>
                            <?php if (is_active_sidebar('iklan_center_right')) : ?>
                                <div class="bg-white">
                                    <?php dynamic_sidebar('iklan_center_right') ?>
                                </div>
                            <?php endif; ?>
                            <?php if (is_active_sidebar('iklan_center_right_2')) : ?>
                                <?php dynamic_sidebar('iklan_center_right_2') ?>
                            <?php endif; ?>
                            <h3 class="page-title">Ikuti Kami</h3>
                            <div class="box p-2 bg-white mb-2">
                                <div class="sosmed">
                                    <a href="<?= ianews_get_theme_option('fb_fan_page') ?>">
                                        <img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/facebook.png" alt="Facebook">
                                    </a>
                                    <a href="<?= ianews_get_theme_option('twitter_url') ?>">
                                        <img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/instagra.png" alt="Twitter">
                                    </a>
                                    <a href="<?= ianews_get_theme_option('ig_url') ?>"><img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/twitter.png" alt="Instagram"></a>
                                    <a href="<?= ianews_get_theme_option('youtube_url') ?>"><img src="<?= get_template_directory_uri() ?>/assets/img/sosmed/youtube.png" alt="Youtube"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
<?php } else :
        get_template_part('content', 'none');
    endif;
}
get_footer(); ?>