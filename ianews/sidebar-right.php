<?php
$kategori = ianews_get_theme_option('sidebar_right_' . $sr);
$side_count = ianews_get_theme_option('sidebar_right_' . $sr . '_value');
$args = array('numberposts' => $side_count ? $side_count : 5, 'category' => $kategori);
$my_posts = get_posts($args); ?>
<h3 class="page-title"><?= get_cat_name($kategori); ?></h3>
<div class="box kanan p-2 bg-white mb-2">
    <?php
    foreach ($my_posts as $sdk => $post) :
        setup_postdata($post); ?>
        <div class="media <?= count($my_posts) - 1 == $sdk ? 'mb-1' : 'mb-2' ?>">
            <?php
            $featured_image_url = wp_get_attachment_url(get_post_thumbnail_id(get_the_ID())); ?>
            <a href="<?= get_the_permalink() ?>" class="mr-3 d-block w-30">
                <?php if (!empty($featured_image_url)) : ?>
                    <img src="<?= $featured_image_url ?>" class="d-block w-100" alt="<?php the_title() ?>">
                <?php else : ?>
                    <img src="<?= get_template_directory_uri() ?>/assets/img/no-image-available.jpg" class="d-block w-100" alt="<?php the_title() ?>">
                <?php endif; ?>
            </a>
            <div class="media-body">
                <h5 class="mt-0 headline_title pb-2"><a href="<?= get_the_permalink() ?>"><?= strlen(get_the_title()) > 60 ? html_cut(get_the_title(), 60) . '...' : the_title() ?></a></h5>
                <div class="sosial_button fs-05">
                    <?= ianews_get_sosial_button('fs-05'); ?>
                </div>
            </div>
        </div>
    <?php endforeach;
    ?>
</div>